package com.yappa.example

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.yappa.sdk.YappaSDK

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        YappaSDK.initialize("aefab81bcc7b2e83d619b6e8f90a6029", 1, this)
        YappaSDK.setContentUrl("https://qa-site.yappaapp.com/qa-demo/")
        YappaSDK.setAppIcon(R.mipmap.ic_launcher)

        Handler(Looper.getMainLooper()).postDelayed({
            YappaSDK.show()

        }, 1000)
    }
}